import Number from '../Number';

import { Component } from 'react'

class NumberList extends Component {
    render(){
        return(
            <div>
                {this.props.list.map((num, index) => {
                    return <Number key={ index } number={ num }/>
                })}
            </div>
        )
    }
} 

export default NumberList;