import { Component } from 'react';

class NumberCreator extends Component {
    state = {
        num: this.props.draft,
    };

    setNumber = (data) => {
        this.setState({ num: data.target.value })
    }

    handleSaveNumber = () => {
        this.props.saveNumber( [ this.state.num] );
        this.setState( { num: 0} );
    }

    render(){
        const { num } = this.state
        return(
            <div>
                <input type="Number" onChange={this.setNumber} value={num}/>
                <button onClick={this.handleSaveNumber}>Salvar</button>
            </div>
        )
    }
}

export default NumberCreator;