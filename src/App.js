import { Component } from 'react';
import './App.css';

import NumberList from './Components/NumberList';
import NumberCreator from './Components/NumberCreator';

class App extends Component {
  state = { numberList:[ null ] }

  saveNumber = (num) => {
    const { numberList } = this.state;
    this.setState({ numberList: [...numberList, num] });
  }

  render(){
    return (
      <div className="App">
        <header className="App-header">
          <NumberCreator 
            saveNumber={this.saveNumber}
          />
          <NumberList list={ this.state.numberList }/>
        </header>
      </div>
    );
  }
}

export default App;
